import random

from flask import Flask, request, jsonify


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py')
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)


    @app.route('/diagnostic', methods=['GET'])
    def diagnostic_route():

        if 'id' in request.args:

            diagnostic = {'id'          : request.args.get('id'),
                          'diagnostico' : random.choice([True, False]),
                          'otro_dato'   : random.random()
                        }

            return jsonify(diagnostic)
        else:
            return 'Error: no id specified', 405


    return app

