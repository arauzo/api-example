from setuptools import find_packages, setup

setup(
    name='api',
    version='0.9.1',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'numpy',
        'flask',
    ],
)
