# Ejemplo API

Ejemplo base de un API web montada con Flask y preparada para producción.


# Test

```
mkdir instance
cp config.py.template instance/config.py
./runDevel.sh
```

y probar conectando a: http://127.0.0.1:5000/diagnostic?id=8


# Docker container

```
python3 setup.py bdist_wheel       # crear paquete instalable con la api

docker build -t img-api-waitress .
docker run -d -p 8558:8558 --name api-waitress --restart=unless-stopped img-api-waitress

# Para depurar
docker logs api-waitress           # para ver el log
docker exec -it api-waitress bash  # para entrar al contenedor
```
