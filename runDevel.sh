#!/bin/bash
echo "If applicable, do not forget to start venv:"
echo "   . venv/bin/activate"
export FLASK_APP=api
export FLASK_ENV=development
#export FLASK_RUN_CERT=adhoc
flask run
