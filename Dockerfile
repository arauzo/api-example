FROM python:3
RUN pip install --upgrade pip
RUN pip install waitress
RUN mkdir -p api-instance
COPY config.py.template /usr/local/var/api-instance/config.py
COPY dist/api-0.9.1-py3-none-any.whl ./
RUN pip install api-0.9.1-py3-none-any.whl
CMD ["waitress-serve", "--port=8558", "--call", "api:create_app"]
